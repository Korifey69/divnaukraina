<?php
//C:\Web\Apache24\htdocs\test\console\controllers\SeedController.php 
//test - название папки проекта
namespace console\controllers;
  
use yii\console\Controller;
use yii\db\mssql\PDO;
  
/**
 * Seed controller
 */
class SeedController extends Controller {
  
    public function actionIndex() {
        $pdo = new PDO('mysql:host=localhost;dbname=tour', 'root', '');
        $seeder = new \tebazil\dbseeder\Seeder($pdo);
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();

        $seeder->table('sightseeings')->columns([
            'id', //automatic pk
            'name'=>$faker->firstName,
            'description'=>$faker->text,
            'shortDescription'=>$faker->text,
            'rlongtitude'=>function() { return rand(1, 180); },
            'rlatitude'=>function() { return rand(1, 180); },
            'fY'=>function() { return rand(1, 180); },
            'fX'=>function() { return rand(1, 180); },
                ])->rowQuantity(2);

        $seeder->refill();
    }
}